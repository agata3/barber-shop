import React from 'react';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import { useTheme } from '@mui/material/styles';
import MenuIcon from '@mui/icons-material/Menu';
import AnchorLink from 'react-anchor-link-smooth-scroll';
import { NavItem } from './components';
import logo from '../../../../svg/illustrations/barbershop.png';

interface Props {
  // eslint-disable-next-line @typescript-eslint/ban-types
  onSidebarOpen: () => void;
  pages: {
    landings: Array<PageItem>;
    company: Array<PageItem>;
    account: Array<PageItem>;
    secondary: Array<PageItem>;
    blog: Array<PageItem>;
    // portfolio: Array<PageItem>;
  };
  colorInvert?: boolean;
}

const Topbar = ({
  onSidebarOpen,
  pages,
  colorInvert = false,
}: Props): JSX.Element => {
  const theme = useTheme();
  const { mode } = theme.palette;
  const {
    landings: landingPages,
    secondary: secondaryPages,
    company: companyPages,
    account: accountPages,
    // portfolio: portfolioPages,
    blog: blogPages,
  } = pages;

  return (
    <>
      {/* <Box zIndex={2000} component="a" href="/">
        <Box position={'absolute'}>
          <Logo
            style={{ position: 'absolute', top: '0' }}
            fill="white"
            height={'150'}
            width={'250'}
          />
        </Box>
      </Box> */}
      <Box
        display={'flex'}
        justifyContent={'space-between'}
        alignItems={'center'}
        width={1}
      >
        <Box
          display={'flex'}
          component="a"
          href="/"
          style={{ textDecoration: 'none' }}
          // title="theFront"
          width={{ xs: 100, md: 120 }}
        >
          <Box
            paddingTop={1}
            component={'img'}
            src={mode === 'light' && !colorInvert ? logo : logo}
            height={1}
            width={1}
          />

          {/* <Typography
          style={
            mode === 'light' && !colorInvert
              ? { textDecoration: 'none', color: '#000' }
              : { textDecoration: 'none', color: '#fff' }
          }
        >
          Logo
        </Typography> */}
        </Box>

        <Box sx={{ display: { xs: 'none', md: 'flex' } }} alignItems={'center'}>
          <Box>
            <AnchorLink
              href="#home"
              style={
                mode === 'light' && !colorInvert
                  ? { textDecoration: 'none', color: '#000' }
                  : { textDecoration: 'none', color: '#fff' }
              }
            >
              <NavItem
                title={'Home'}
                id={'landing-pages'}
                items={landingPages}
                colorInvert={colorInvert}
              />
            </AnchorLink>
          </Box>
          <Box marginLeft={4}>
            <AnchorLink
              href="#about-us"
              style={
                mode === 'light' && !colorInvert
                  ? { textDecoration: 'none', color: '#000' }
                  : { textDecoration: 'none', color: '#fff' }
              }
            >
              <NavItem
                title={'About Us'}
                id={'company-pages'}
                items={companyPages}
                colorInvert={colorInvert}
              />
            </AnchorLink>
          </Box>
          <Box marginLeft={4}>
            <AnchorLink
              href="#services"
              style={
                mode === 'light' && !colorInvert
                  ? { textDecoration: 'none', color: '#000' }
                  : { textDecoration: 'none', color: '#fff' }
              }
            >
              <NavItem
                title={'Services'}
                id={'secondary-pages'}
                items={secondaryPages}
                colorInvert={colorInvert}
              />
            </AnchorLink>
          </Box>
          <Box marginLeft={4}>
            <AnchorLink
              href="#gallery"
              style={
                mode === 'light' && !colorInvert
                  ? { textDecoration: 'none', color: '#000' }
                  : { textDecoration: 'none', color: '#fff' }
              }
            >
              <NavItem
                title={'Gallery'}
                id={'account-pages'}
                items={accountPages}
                colorInvert={colorInvert}
              />
            </AnchorLink>
          </Box>

          <Box marginLeft={4}>
            <AnchorLink
              href="#contact"
              style={
                mode === 'light' && !colorInvert
                  ? { textDecoration: 'none', color: '#000' }
                  : { textDecoration: 'none', color: '#fff' }
              }
            >
              <NavItem
                title={'Contact us'}
                id={'blog-pages'}
                items={blogPages}
                colorInvert={colorInvert}
              />
            </AnchorLink>
          </Box>
          {/* <Box marginLeft={4}>
          <NavItem
            title={'Portfolio'}
            id={'portfolio-pages'}
            items={portfolioPages}
            colorInvert={colorInvert}
          />
        </Box> */}
          {/* <Box marginLeft={4}>
          <Button
            variant="contained"
            color="primary"
            component="a"
            target="blank"
            href="https://mui.com/store/items/the-front-landing-page/"
            size="large"
          >
            Buy now
          </Button>
        </Box> */}
        </Box>
        <Box sx={{ display: { xs: 'flex', md: 'none' } }} alignItems={'center'}>
          <Button
            onClick={() => onSidebarOpen()}
            aria-label="Menu"
            variant={'outlined'}
            style={
              mode === 'light' && !colorInvert
                ? { textDecoration: 'none', borderColor: '#000' }
                : { textDecoration: 'none', borderColor: '#fff' }
            }
            sx={{
              borderRadius: 2,
              minWidth: 'auto',
              padding: 1,
              // borderColor: alpha(theme.palette.divider, 0.2),
              borderColor: '#000',
            }}
          >
            <MenuIcon
              style={
                mode === 'light' && !colorInvert
                  ? { textDecoration: 'none', color: '#000' }
                  : { textDecoration: 'none', color: '#fff' }
              }
            />
          </Button>
        </Box>
      </Box>
    </>
  );
};

export default Topbar;
