import React from 'react';
import Box from '@mui/material/Box';
import { useTheme } from '@mui/material/styles';
import AnchorLink from 'react-anchor-link-smooth-scroll';
import NavItem from './components/NavItem';
import logo from '../../../../../../svg/illustrations/barbershop.png';

interface Props {
  pages: {
    landings: Array<PageItem>;
    company: Array<PageItem>;
    account: Array<PageItem>;
    secondary: Array<PageItem>;
    blog: Array<PageItem>;
    portfolio: Array<PageItem>;
  };
}

const SidebarNav = ({ pages }: Props): JSX.Element => {
  const theme = useTheme();
  const { mode } = theme.palette;

  const {
    landings: landingPages,
    secondary: secondaryPages,
    company: companyPages,
    account: accountPages,
    // portfolio: portfolioPages,
    blog: blogPages,
  } = pages;

  return (
    <Box>
      <Box width={1} paddingX={2} paddingY={2}>
        <Box
          display={'flex'}
          component="a"
          href="/"
          // title="theFront"
          style={{ textDecoration: 'none', color: '#000' }}
          width={{ xs: 100, md: 120 }}
        >
          <Box
            component={'img'}
            src={mode === 'light' ? logo : logo}
            height={1}
            width={1}
          />

          {/* <Logo fill="white" /> */}
        </Box>
      </Box>
      <Box paddingX={2} paddingY={2}>
        <Box paddingY={2}>
          <AnchorLink href="#home" style={{ textDecoration: 'none' }}>
            <NavItem title={'Home'} items={landingPages} />
          </AnchorLink>
        </Box>
        <Box paddingY={2}>
          <AnchorLink href="#about-us" style={{ textDecoration: 'none' }}>
            <NavItem title={'About us'} items={companyPages} />
          </AnchorLink>
        </Box>
        <Box paddingY={2}>
          <AnchorLink href="#services" style={{ textDecoration: 'none' }}>
            <NavItem title={'Services'} items={secondaryPages} />
          </AnchorLink>
        </Box>
        <Box paddingY={2}>
          <AnchorLink href="#gallery" style={{ textDecoration: 'none' }}>
            <NavItem title={'Gallery'} items={accountPages} />
          </AnchorLink>
        </Box>
        <Box paddingY={2}>
          <AnchorLink href="#contact" style={{ textDecoration: 'none' }}>
            <NavItem title={'Contact'} items={blogPages} />
          </AnchorLink>
        </Box>
        {/* <Box>
          <NavItem title={'Portfolio'} items={portfolioPages} />
        </Box> */}
        {/* <Box marginTop={2}> */}
        {/* <Button
            size={'large'}
            variant="outlined"
            fullWidth
            component="a"
            href="/docs/introduction"
          >
            Documentation
          </Button>
        </Box> */}
        {/* <Box marginTop={1}>
          <Button
            size={'large'}
            variant="contained"
            color="primary"
            fullWidth
            component="a"
            target="blank"
            href="https://mui.com/store/items/the-front-landing-page/"
          >
            Purchase now
          </Button> */}
        {/* </Box> */}
      </Box>
    </Box>
  );
};

export default SidebarNav;
