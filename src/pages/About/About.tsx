/* eslint-disable react/no-unescaped-entities */
import React from 'react';
import { LazyLoadImage } from 'react-lazy-load-image-component';
import { useTheme } from '@mui/material/styles';
// import { colors } from '@mui/material';
import Box from '@mui/material/Box';
import Grid from '@mui/material/Grid';
import Typography from '@mui/material/Typography';
import useMediaQuery from '@mui/material/useMediaQuery';
import about from '../../svg/illustrations/about.jpg';

export const About = (): JSX.Element => {
  const theme = useTheme();
  const isMd = useMediaQuery(theme.breakpoints.up('md'), {
    defaultMatches: true,
  });

  return (
    <Box
      marginTop={{ xs: 4, sm: 4, md: 3 }}
      padding={{ xs: 2, sm: 4 }}
      borderRadius={2}
      style={{
        backgroundColor: '#1a2137',
      }}
      // bgcolor={theme.palette.mode === 'light' ? colors.blue[50] : '#222B45'}
      data-aos={'fade-up'}
    >
      <Grid
        container
        spacing={isMd ? 4 : 2}
        flexDirection={{ xs: 'column-reverse', md: 'row' }}
      >
        <Grid item xs={12} md={8}>
          <Grid container spacing={isMd ? 4 : 2}>
            <Grid
              item
              xs={12}
              sx={{
                '& .lazy-load-image-background.lazy-load-image-loaded': {
                  width: '100%',
                  height: '100%',
                },
              }}
            >
              <Box
                component={LazyLoadImage}
                height={1}
                width={1}
                src={`${about}`}
                alt="..."
                effect="blur"
                borderRadius={2}
                maxWidth={1}
                maxHeight={400}
                sx={{
                  objectFit: 'cover',
                }}
              />
            </Grid>
            <Grid item xs={6}>
              <Typography variant={'h6'} fontWeight={700} gutterBottom>
                Loyal well-trained staff
              </Typography>
              <Typography component={'p'}>
                The Barber name specialise in all aspects of cutting, restyling
                and colouring of men’s hair – along with traditional open razor
                shaving.
              </Typography>
            </Grid>
            <Grid item xs={6}>
              <Typography variant={'h6'} fontWeight={700} gutterBottom>
                Trust the professionals
              </Typography>
              <Typography component={'p'}>
                The Barber and experience the service for yourself, you will
                find traditional reclining barber chairs, front wash ceramic
                sink marble tops, reclaimed floors, New-York style tin ceilings,
                deep button leather waiting areas, flat screen TVs and
                PlayStations for the kids.
              </Typography>
            </Grid>
          </Grid>
        </Grid>
        <Grid item xs={12} md={4}>
          <Box
            display={'flex'}
            flexDirection={'column'}
            justifyContent={{ xs: 'flex-start', md: 'space-between' }}
            height={1}
          >
            <Box>
              <Typography
                variant={'h6'}
                sx={{
                  fontWeight: 'medium',
                }}
                gutterBottom
                color={'text.secondary'}
                // align={'center'}
              >
                More than 10 years in business
              </Typography>
              <Typography variant={'h3'} fontWeight={700} gutterBottom>
                ABOUT US
              </Typography>
              <Typography
                variant={'h5'}
                color={
                  theme.palette.mode === 'light'
                    ? 'text.secondary'
                    : 'text.primary'
                }
              >
                We are empowering and developing every gentleman’s distinct
                style for over 10 years of experience
                <br />
                in the barber industry and we know exactly how to make your
                first impressions count.
              </Typography>
            </Box>
          </Box>
        </Grid>
      </Grid>
    </Box>
  );
};

export default About;
