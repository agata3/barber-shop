import React from 'react';
// import { useTheme } from '@mui/material/styles';
import Card from '@mui/material/Card';
import Box from '@mui/material/Box';
import CardContent from '@mui/material/CardContent';
import Typography from '@mui/material/Typography';
import Avatar from '@mui/material/Avatar';
import Grid from '@mui/material/Grid';
import ListItem from '@mui/material/ListItem';
import ListItemText from '@mui/material/ListItemText';
import ListItemAvatar from '@mui/material/ListItemAvatar';
import { colors } from '@mui/material';
const mock = [
  {
    title: 'HAIRCUTS ',
    price: ' FROM €18',
    features: [
      {
        title: 'GENTS HAIRCUT & STYLING ',
        price: '€25',
        subtitle: 'Includes ear hair singeing, eyebrows trim, gel/wax',
      },
      {
        title: 'WASH, CUT, BLOW DRY & STYLING  ',
        price: '€35',
        subtitle: 'Hair conditioner and head massage included',
      },
      { title: 'BOYS HAIRCUT  ', price: '€18', subtitle: 'Under 15 years old' },
    ],
    isHighlighted: true,
  },
  {
    title: 'SHAVES',
    price: 'FROM €44',
    features: [
      {
        title: 'HOT TOWEL SHAVE  ',
        subtitle: 'By appointment only',
        price: '€26',
      },
      {
        title: 'HEAD SHAVE  ',
        subtitle: 'Head shave with clippers',
        price: '€13',
      },
      {
        title: 'SPECIAL HEAD SHAVE  ',
        price: '€21',
        subtitle: 'Head shave a with cut-throat razor or foil shaver',
      },
      {
        title: 'SKIN FADE  ',
        price: '€25',
        subtitle: 'With a cut-throat razor or foil shaver',
      },
    ],
    isHighlighted: true,
  },
  {
    title: 'STYLING',

    features: [
      { title: 'WASH  ', price: '€5' },
      { title: 'BEARD TRIM  ', price: '€8' },
    ],
    isHighlighted: true,
  },
];

const Services = (): JSX.Element => {
  // const theme = useTheme();

  return (
    <Box paddingTop={{ xs: 10, sm: 12, md: 15 }}>
      <Box marginBottom={4}>
        <Typography
          variant={'h4'}
          color={'text.primary'}
          align={'center'}
          gutterBottom
          sx={{ fontWeight: 700 }}
        >
          OUR PRICES
        </Typography>
        <Typography
          variant={'h6'}
          component={'p'}
          color={'text.secondary'}
          align={'center'}
        >
          The Best Barber Experience
        </Typography>
      </Box>
      <Grid container spacing={4}>
        {mock.map((item, i) => (
          <Grid item xs={12} md={4} key={i}>
            <Box
              component={Card}
              height={1}
              display={'flex'}
              flexDirection={'column'}
              boxShadow={item.isHighlighted ? 4 : 0}
            >
              <CardContent
                sx={{
                  padding: 4,
                }}
              >
                <Box
                  marginBottom={4}
                  display={'flex'}
                  justifyContent={'space-between'}
                >
                  <Typography variant={'h4'}>
                    <Box fontWeight={600}>{item.title}</Box>
                  </Typography>
                  <Box display={'flex'}>
                    <Typography variant={'h5'} color={'secondary'}>
                      <Box fontWeight={400}>{item.price}</Box>
                    </Typography>
                  </Box>
                </Box>
                <Grid container spacing={1}>
                  {item.features.map((feature, j) => (
                    <Grid item xs={12} key={j} style={{ width: '100%' }}>
                      <Box
                        display={'flex'}
                        component={ListItem}
                        disableGutters
                        width={'auto'}
                        padding={0}
                      >
                        <Box
                          component={ListItemAvatar}
                          minWidth={'auto !important'}
                          marginRight={2}
                        >
                          <Box
                            component={Avatar}
                            bgcolor={colors.yellow[500]}
                            width={12}
                            height={12}
                          >
                            <svg
                              width={10}
                              height={10}
                              xmlns="http://www.w3.org/2000/svg"
                              viewBox="0 0 20 20"
                              fill="currentColor"
                            >
                              <path
                                fillRule="evenodd"
                                d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z"
                                clipRule="evenodd"
                              />
                            </svg>
                          </Box>
                        </Box>

                        <Box flexGrow={1}>
                          <ListItemText>
                            <Typography fontWeight={600}>
                              {feature.title}
                            </Typography>
                          </ListItemText>
                        </Box>
                        <Box>
                          <Typography color={'secondary'}>
                            <span
                              style={{
                                fontWeight: '700',
                                color: 'secondary',
                                fontSize: '18px',
                              }}
                            >
                              {feature.price}
                            </span>
                          </Typography>
                        </Box>
                      </Box>

                      <Box textAlign={'left'} marginBottom={2} marginX={3.5}>
                        <ListItemText secondary={feature.subtitle} />
                      </Box>
                    </Grid>
                  ))}
                </Grid>
              </CardContent>
              <Box flexGrow={1} />
            </Box>
          </Grid>
        ))}
      </Grid>
    </Box>
  );
};

export default Services;
