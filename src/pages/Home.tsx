// import Divider from '@mui/material/Divider';
import { Box, Divider } from '@mui/material';
import Container from 'components/Container';
import { Main } from 'layouts';
import Contact from './Contact/Contact';
import { About } from './About/About';
import Reviews from './Reviews/Reviews';
import Services from './Services/Services';
import Gallery from './Gallery/Gallery';
// import Carousel from './Carousel/Carousel';
import Hero from './Hero/Hero';

export const Home = () => {
  return (
    <Main colorInvert={true}>
      <section id="home">
        <Hero />
      </section>
      <section id="about-us">
        <Container>
          <About />
        </Container>
        <Box bgcolor={'alternate.main'}>
          <Container>
            <Reviews />
          </Container>
        </Box>
      </section>
      <Container>
        <section id="services">
          <Services />
        </section>
        <section id="gallery">
          <Gallery />
          {/* <Carousel /> */}
        </section>
      </Container>
      <section id="contact">
        <Divider />

        <Contact />
      </section>
    </Main>
  );
};
