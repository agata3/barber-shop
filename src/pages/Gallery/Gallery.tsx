import React, { useState } from 'react';
import { LazyLoadImage } from 'react-lazy-load-image-component';
import Lightbox from 'react-image-lightbox';
import { useTheme } from '@mui/material/styles';
import useMediaQuery from '@mui/material/useMediaQuery';
import Box from '@mui/material/Box';
import ImageList from '@mui/material/ImageList';
import ImageListItem from '@mui/material/ImageListItem';
import Typography from '@mui/material/Typography';
import gallery1 from '../../svg/illustrations/gallery1.jpg';
import gallery2 from '../../svg/illustrations/gallery2.jpg';
import gallery3 from '../../svg/illustrations/gallery3.jpg';
import gallery4 from '../../svg/illustrations/gallery4.jpg';
import gallery5 from '../../svg/illustrations/gallery5.jpg';
import gallery6 from '../../svg/illustrations/gallery6.jpg';
import gallery7 from '../../svg/illustrations/gallery7.jpg';
import gallery8 from '../../svg/illustrations/gallery8.jpg';
import gallery9 from '../../svg/illustrations/gallery9.jpg';
import gallery10 from '../../svg/illustrations/gallery10.jpg';

const Gallery = (): JSX.Element => {
  const theme = useTheme();
  const [currentImage, setCurrentImage] = useState(0);
  const [viewerIsOpen, setViewerIsOpen] = useState(false);

  const openLightbox = (index: number): void => {
    setCurrentImage(index);
    setViewerIsOpen(true);
  };

  const closeLightbox = (): void => {
    setCurrentImage(0);
    setViewerIsOpen(false);
  };

  const isMd = useMediaQuery(theme.breakpoints.up('md'), {
    defaultMatches: true,
  });

  const photos = [
    {
      src: gallery1,
      rows: 1,
      cols: 1,
    },
    {
      src: gallery2,
      rows: 1,
      cols: 1,
    },
    {
      src: gallery3,
      rows: 1,
      cols: 1,
    },
    {
      src: gallery10,
      rows: 1,
      cols: 1,
    },
    {
      src: gallery4,
      rows: 1,
      cols: 2,
    },
    {
      src: gallery6,
      rows: 1,
      cols: 1,
    },
    {
      src: gallery7,
      rows: 1,
      cols: 1,
    },
    {
      src: gallery8,
      rows: 1,
      cols: 1,
    },
    {
      src: gallery9,
      rows: 1,
      cols: 1,
    },
    {
      src: gallery5,
      rows: 1,
      cols: 2,
    },
  ];

  const photosToShow = isMd ? photos : photos.slice(0, photos.length - 1);

  return (
    <Box paddingTop={{ xs: 12, sm: 14, md: 22 }}>
      <Box marginBottom={3}>
        <Typography
          sx={{
            fontWeight: 'medium',
          }}
          gutterBottom
          color={'text.secondary'}
          align={'center'}
        >
          More than 10 years in business
        </Typography>
        <Typography
          variant="h4"
          align={'center'}
          data-aos={'fade-up'}
          gutterBottom
          sx={{
            fontWeight: 700,
          }}
        >
          GALLERY
        </Typography>
      </Box>
      <Box paddingLeft={0.9} paddingRight={0.9} marginBottom={-1}>
        <ImageList
          variant="quilted"
          cols={4}
          rowHeight={isMd ? 300 : 220}
          gap={isMd ? 14 : 8}
        >
          {photosToShow.map((item, i) => (
            <ImageListItem
              key={i}
              cols={isMd ? item.cols || 1 : 4}
              rows={isMd ? item.rows || 1 : 2}
            >
              <LazyLoadImage
                height={'100%'}
                width={'100%'}
                src={item.src}
                alt="..."
                effect="blur"
                onClick={() => openLightbox(i)}
                style={{
                  objectFit: 'cover',
                  cursor: 'poiner',
                  borderRadius: 8,
                }}
              />
            </ImageListItem>
          ))}
        </ImageList>
      </Box>
      {viewerIsOpen && (
        <Lightbox
          mainSrc={photos[currentImage].src}
          nextSrc={photos[(currentImage + 1) % photos.length].src}
          prevSrc={
            photos[(currentImage + photos.length - 1) % photos.length].src
          }
          onCloseRequest={() => closeLightbox()}
          onMovePrevRequest={() =>
            setCurrentImage((currentImage + photos.length - 1) % photos.length)
          }
          onMoveNextRequest={() =>
            setCurrentImage((currentImage + 1) % photos.length)
          }
          reactModalStyle={{ overlay: { zIndex: 1500 } }}
        />
      )}
    </Box>
  );
};

export default Gallery;
