import React from 'react';
import Grid from '@mui/material/Grid';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import { useTheme } from '@mui/material/styles';
import logo from '../../svg/illustrations/barbershop.png';
const Footer = (): JSX.Element => {
  const theme = useTheme();
  const { mode } = theme.palette;

  return (
    <Grid container spacing={2}>
      <Grid item xs={12}>
        <Box
          display={'flex'}
          // justifyContent={'space-between'}
          alignItems={'center'}
          width={1}
          flexDirection={{ xs: 'column', sm: 'row' }}
          justifyContent={'center'}
        >
          <Box
            component="a"
            href="/"
            style={{ textDecoration: 'none' }}
            width={{ xs: 100, md: 120 }}
            display={'flex'}

            // title="theFront"
            // width={80}
          >
            <Box
              component={'img'}
              src={mode === 'light' ? logo : logo}
              height={1}
              width={1}
            />
            {/* <Logo fill="white" /> */}
          </Box>
        </Box>
      </Grid>
      <Grid item xs={12}>
        <Typography
          align={'center'}
          variant={'subtitle2'}
          color="text.secondary"
          gutterBottom
        >
          &copy; theBarberShop. 2022.
        </Typography>
        {/* <Typography
          align={'center'}
          variant={'caption'}
          color="text.secondary"
          component={'p'}
        >
          Designed and hosted by Agata Szczypta
        </Typography> */}
      </Grid>
    </Grid>
  );
};

export default Footer;
