import Box from '@mui/material/Box';
import Container from 'components/Container';
import Main from 'layouts/Main';
import { Hero } from './components/Hero';
import { Services } from './components/Services';
import { LastStories } from './components/LastStories';
import { Stories } from './components/Stories';
import { Partners } from './components/Partners';
import { Solutions } from './components/Solutions';
export const Test = () => {
  return (
    <Main colorInvert={true}>
      <Hero />
      <Container paddingY={'0 !important'}>
        <Box bgcolor={'alternate.main'}>
          <Container>
            <Container>
              <Solutions />
            </Container>
            <Container>
              <Stories />
            </Container>
            <Container>
              <Services />
            </Container>
            <Partners />
            <Container paddingTop={15}>
              <LastStories />
            </Container>
          </Container>
        </Box>
      </Container>
    </Main>
  );
};
